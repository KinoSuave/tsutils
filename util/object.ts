export const isDefinedWithPropsOfType = (
	obj: Record<string, unknown>,
	props: string[],
	type: string
): boolean => {
	return (
		obj != null &&
		props.every((prop) => obj.hasOwnProperty(prop) && typeof prop === type)
	);
};

export const isDefinedWithProps = (
	obj: Record<string, unknown>,
	props: string[]
): boolean => {
	return obj != null && props.every((prop) => obj.hasOwnProperty(prop));
};

export const copyDefinedProps = (
	obj: Record<string, unknown>,
	target: any,
	props: string[]
): any => {
	props.forEach((propName) => {
		if (obj[propName] !== undefined) {
			target[propName] = obj[propName];
		}
	});

	return target;
};
