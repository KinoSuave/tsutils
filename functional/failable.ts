/* eslint-disable max-classes-per-file */
export interface IFailable<ST, FT = any> {
	flatMap<K>(f: (value: ST) => IFailable<K, FT>): IFailable<K, FT>;
	fold<K>(f: (exception: FT) => K, s: (value: ST) => K): K;
	foreach(f: (value: ST) => void): void;
	get(): ST;
	getException(): FT;
	getOr<K>(orValue: K): ST | K;
	getOrElse<K>(f: () => K): ST | K;
	isFailure(): boolean;
	isSuccess(): boolean;
	or(orValue: ST): IFailable<ST, any>;
	map<K>(f: (value: ST) => K): IFailable<K, FT>;
	recover(f: (exception: FT) => ST): IFailable<ST, FT>;
	recoverWith(f: (exception: FT) => IFailable<ST, FT>): IFailable<ST, FT>;
	transformFailure<K>(f: (value: FT) => K): IFailable<ST, K>;
}

// Note: You have to know the failure (thrown) type if using this
export const failableWithCatch = <ST, FT>(f: () => ST): IFailable<ST, FT> => {
	try {
		return Success(f());
	} catch (exception) {
		return Failure(exception) as IFailable<ST, FT>;
	}
};

// Ditto as above, you have to know the failure type if using this
export const asyncFailableWithCatch = <ST, FT>(
	f: () => Promise<ST>
): Promise<IFailable<ST, FT>> => {
	return f()
		.then((value) => {
			return Success(value);
		})
		.catch((exception) => {
			return Failure(exception);
		});
};

export class FAILURE<ST, FT = any> implements IFailable<ST, FT> {
	constructor(private readonly exception: FT) {}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public flatMap<K>(f: (value: ST) => IFailable<K, FT>): IFailable<any, FT> {
		return this;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public fold<K>(f: (exception: FT) => K, s: (value: ST) => K): K {
		return f(this.exception);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public foreach(f: (value: ST) => void): void {}

	public get(): ST {
		throw this.exception;
	}

	public getException(): FT {
		return this.exception;
	}

	public getOr<K>(orValue: K): K {
		return orValue;
	}

	public getOrElse<K>(f: () => K): K {
		return f();
	}

	public isFailure(): boolean {
		return true;
	}

	public isSuccess(): boolean {
		return false;
	}

	public or(orValue: ST): IFailable<ST, any> {
		return Success(orValue);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public map<K>(f: (value: ST) => K): IFailable<any, FT> {
		return this;
	}

	public recover(f: (exception: FT) => ST): IFailable<ST, FT> {
		return Success(f(this.exception));
	}

	public recoverWith(
		f: (exception: FT) => IFailable<ST, FT>
	): IFailable<ST, FT> {
		return f(this.exception);
	}

	public transformFailure<K>(f: (value: FT) => K): IFailable<ST, K> {
		return Failure(f(this.exception));
	}
}

export class SUCCESS<ST, FT = any> implements IFailable<ST, FT> {
	constructor(private readonly value: ST) {}

	public flatMap<K>(f: (value: ST) => IFailable<K, FT>): IFailable<any, FT> {
		return f(this.value);
	}

	public fold<K>(f: (exception: FT) => K, s: (value: ST) => K): K {
		return s(this.value);
	}

	public foreach(f: (value: ST) => void): void {
		f(this.value);
	}

	public get(): ST {
		return this.value;
	}

	public getException(): FT {
		throw new Error("No error value");
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public getOr<K>(orValue: K): ST {
		return this.value;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public getOrElse<K>(f: () => K): ST {
		return this.value;
	}

	public isFailure(): boolean {
		return false;
	}

	public isSuccess(): boolean {
		return true;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public or(orValue: ST): IFailable<ST, any> {
		return this;
	}

	public map<K>(f: (value: ST) => K): IFailable<K, FT> {
		return Success(f(this.value));
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public recover(f: (exception: FT) => ST): IFailable<ST, FT> {
		return this;
	}

	public recoverWith(
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		f: (exception: FT) => IFailable<ST, FT>
	): IFailable<ST, FT> {
		return this;
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public transformFailure<K>(f: (value: FT) => K): IFailable<ST, K> {
		return this as unknown as IFailable<ST, K>;
	}
}

export const Failure = <FT>(exception: FT): FAILURE<any, FT> => {
	return new FAILURE<any, FT>(exception);
};

export const Success = <ST>(value: ST): SUCCESS<ST, any> => {
	return new SUCCESS<ST, any>(value);
};
